import logo from './logo.svg';
import './App.css';
import SignUp from './signup'
import Login from "./login"
import {BrowserRouter as Router,Switch, Route} from 'react-router-dom'
function App() {
  return (
    <Router>
    <div className="App">
    <Switch>
      <Route exact path="/signup" component={SignUp} />
      <Route exact path="/users/:userid/login" component={Login} />
    </Switch>
    </div>
    </Router>
  );
}

export default App;
