import React from "react";
import axios from "axios";
import {Link,Router} from 'react-router-dom'
class SignUp extends React.Component{
    constructor(){
        super()
        this.state = {
            firstname:'',
            lastname:'',
            email:'',
            password:''
        }
    }
    onChange = (e) =>{
        this.setState({[e.target.name] : e.target.value})
        console.log(this.state)
    }
    handleSubmit = (e)=>{
        e.preventDefault()
        const data={
            firstname : this.state.firstname,
            lastname :this.state.lastname,
            email : this.state.email,
            password : this.state.password
        }

        console.log(data)
        axios.post('http://localhost:3030/signup',data).then(response =>{
            console.log(response.data)
        }).catch(err=>{
            console.error(err)
        })
    }
    render(){
        const { firstname , lastname, email , password} = this.state
        return(
        <div>
            <h1>Sign Up</h1>
            <form onSubmit={this.handleSubmit}>
                <label htmlFor='fname'>First Name</label>
                <input
                    type='text'
                    name='firstname'
                    value={firstname}
                    placeholder='Enter Firstname'
                    onChange={this.onChange}
                />
                <label htmlFor='lname'>Last Name</label>
                 <input
                    type='text'
                    name='lastname'
                    value={lastname}
                    placeholder='Enter lastname'
                    onChange={this.onChange}
                />
                <label htmlFor='email'>Email</label>
                 <input
                    type='text'
                    name='email'
                    value={email}
                    placeholder='Enter Email'
                    onChange={this.onChange}
                />
                <label htmlFor='password'>Password</label>
                 <input
                    type='text'
                    name='password'
                    value={password}
                    placeholder='Enter Password'
                    onChange={this.onChange}
                />
                <button>
                    SignUp
                </button>
                <p>Already a user</p>
            </form>
        </div>
        )
    }
}

export default SignUp
