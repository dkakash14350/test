import React from "react"
import axios from 'axios'
import {Redirect} from 'react-router-dom'
class Login extends React.Component{
    constructor(){
        super()
        this.state={
            email:'',
            password:''
        }
    }
    onChange = (e) =>{
        this.setState({[e.target.name] : e.target.value})
        console.log(this.state)
    }
    handleSubmit = (e) =>{
        e.preventDefault()
        const data = {
            password: this.state.password
        }
        axios.post(`http://localhost:3030/users/${this.state.email}/login`, data)
        .then(response=>{console.log(response)})
        .catch(err=>{
            console.error(err)
        })
    }
    render(){
        const { email , password} = this.state
        return(
            <div>
                <h1>Login</h1>
                <form onSubmit= {this.handleSubmit}>
                <label htmlFor='email'>Email</label>
                 <input
                    type='text'
                    name='email'
                    value={email}
                    placeholder='Enter Email'
                    onChange={this.onChange}
                />
                <label htmlFor='password'>Password</label>
                 <input
                    type='text'
                    name='password'
                    value={password}
                    placeholder='Enter Password'
                    onChange={this.onChange}
                />
                <button>
                    Login
                </button>
                <p>Not SignedUp</p><p>signup </p> 
                </form>
            </div>
        )
    }
}

export default Login